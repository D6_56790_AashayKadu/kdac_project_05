package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojos.*;
import static utils.DBUtils.*;



public class userAccount {
	
	private Connection cn;
	private PreparedStatement pst1,pst2,pst3;
	
	public userAccount() throws ClassNotFoundException, SQLException {
		
		cn = fetchConnection();
		String sql ="select email,password from users where email=? and password=?";
		pst1 = cn.prepareStatement("insert into users values(default,?,?,?,?,?,?,?)");
		pst2 = cn.prepareStatement(sql);
//		pst2 = cn.prepareStatement("select email,password from users where email=? and password=?");
		pst3 = cn.prepareStatement("update users set password=?, where id=?");
//		pst4=cn.prepareStatement("delete from my_emp where empid=?");
//		System.out.println("emp dao created...");
		

	}

	
	public void cleanUp() throws SQLException {
		if (pst1 != null)
		
			cn.close();
		System.out.println("user dao cleaned up !");
	}

	
	public String addEmpDetails(User us) throws SQLException {
		
		pst1.setString(1, us.getEmail());
		pst1.setString(2, us.getPassword());
		pst1.setString(3, us.getMobile());
		pst1.setString(4, us.getName());
		pst1.setString(5, us.getGender());
		pst1.setString(6, us.getAddress());
		pst1.setDate(7, us.getBirth_date());
		
		int updateCount = pst1.executeUpdate();
		if (updateCount == 1)
			return "users details added....";

		return "Adding users details failed...";
	}
	public List<User> signin(String email,String password) throws SQLException{
		List<User> user = new ArrayList<>();
		
		pst2.setString(1, email);
		pst2.setString(2, password);
		
		try (ResultSet rst = pst2.executeQuery()) {
			while (rst.next())
				user.add(new User(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4)));

		} 
		return user;
	}
	
	public String updateUser(int id,String password) throws SQLException{
		pst3.setString(1, password);
		pst3.setInt(2, id);
		int updateCount = pst3.executeUpdate();
		if (updateCount == 1)
			return "User details updated....";
		return "Updating User details failed...";
	}

}
