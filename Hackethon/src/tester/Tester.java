package tester;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import pojos.*;

import dao.userAccount;

public class Tester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try (Scanner sc = new Scanner(System.in)) {
			// create single dao instance : init phase of the app
			userAccount dao = new userAccount();
			
			boolean exit = false;
			while (!exit) {
				// clnt request servicing phase
				System.out.println(
						"1.sign up");
				try {
					switch (sc.nextInt()) {
					
					case 1:
						System.out
								.println("Enter user details : email,  password,  mobile,  name,  gender,address, birth_date (yr-mon-day)");
						System.out.println(dao.addEmpDetails(new User(sc.next(), sc.next(), sc.next(),sc.next(),sc.next(),sc.next(),
								 Date.valueOf(sc.next()))));
						break;
					case 2:
						
						System.out.println(dao.signin(sc.next(), sc.next()));
						System.out.println("Sign in successful");
						break;
					
					case 3:
						System.out.println("Enter id and password");
						System.out.println(dao.updateUser(sc.nextInt(), sc.next()));
						System.out.println("Edit successfull");
						break;
					case 5:
						exit = true;
						// clean up db resources
						dao.cleanUp();
						break;
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				sc.nextLine();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		}

	}


