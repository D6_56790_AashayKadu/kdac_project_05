package pojos;
import java.sql.Date;

public class User {
	private int id;
	private String email;
	private String password;
	private String mobile;
	private String name;
	private String gender;
	private String address;
	private Date birth_date;
	public User(int id, String email, String password, String mobile, String name, String gender, String address,
			Date birth_date) {
		this.id = id;
		this.email = email;
		this.password = password;
		this.mobile = mobile;
		this.name = name;
		this.gender = gender;
		this.address = address;
		this.birth_date = birth_date;

	}
	
	public User(String email, String password, String mobile, String name, String gender, String address,
			Date birth_date) {
		this.email = email;
		this.password = password;
		this.mobile = mobile;
		this.name = name;
		this.gender = gender;
		this.address = address;
		this.birth_date = birth_date;
	}
	

	public User(int id, String email, String name, String address) {
		super();
		this.id = id;
		this.email = email;
		this.name = name;
		this.address = address;
	}
	

	public User(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getBirth_date() {
		return birth_date;
	}
	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}
	@Override
	public String toString() {
		return "User [address=" + address + ", birth_date=" + birth_date + ", email=" + email + ", gender=" + gender
				+ ", id=" + id + ", mobile=" + mobile + ", name=" + name + ", password=" + password + "]";
	}
	
	
	
	/* @Override
	public boolean equals(Object o) {
		System.out.println("in User equals");
		if (o instanceof User)
			return this.id.equals(((User) o).id);
		return false;
	}
	*/
	
	
}
