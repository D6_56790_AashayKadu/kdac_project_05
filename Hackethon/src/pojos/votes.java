package pojos;

import java.sql.Date;

public class votes {
	
	
	
//	-------------------+-------------+------+-----+---------+----------------+   
//	 Field             | Type        | Null | Key | Default | Extra          |   
//	-------------------+-------------+------+-----+---------+----------------+   
//	 id                | int         | NO   | PRI | NULL    | auto_increment |   
//	 que_id            | int         | YES  | MUL | NULL    |                |   
//	 user_id           | int         | YES  | MUL | NULL    |                |   
//	 selected_option   | varchar(50) | YES  |     | NULL    |                |   
//	 answered_datetime | datetime    | YES  |     | NULL    |                |   
//	-------------------+-------------+------+-----+---------+----------------+   


	private int id;
	private int que_id;
	private int user_id;
	private String selected_option;
	
	private Date answered_datetime;

	public votes(int id, int que_id, int user_id, String selected_option, Date answered_datetime) {
		super();
		this.id = id;
		this.que_id = que_id;
		this.user_id = user_id;
		this.selected_option = selected_option;
		this.answered_datetime = answered_datetime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQue_id() {
		return que_id;
	}

	public void setQue_id(int que_id) {
		this.que_id = que_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getSelected_option() {
		return selected_option;
	}

	public void setSelected_option(String selected_option) {
		this.selected_option = selected_option;
	}

	public Date getAnswered_datetime() {
		return answered_datetime;
	}

	public void setAnswered_datetime(Date answered_datetime) {
		this.answered_datetime = answered_datetime;
	}

	@Override
	public String toString() {
		return "votes [id=" + id + ", que_id=" + que_id + ", user_id=" + user_id + ", selected_option="
				+ selected_option + ", answered_datetime=" + answered_datetime + "]";
	} 

	

}
